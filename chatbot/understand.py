TERMS = ["hi", "hello", "list", "of", "devices"]

NON_TERMS = ["HELLO", "LIST_OF_DEVICES", "LIST", "ARTICLE", "DEVICES"]

table = {
    "HELLO": {
        "END": -1,
        "INVALID": -1,
        "hi": "<HELLO> -> hi",
        "hello": "<HELLO> -> hello",
        "list": -1,
        "of": -1,
        "devices": -1
        },
}

reverse_table = {
    "hi": "HELLO",
    "hello": "HELLO",
    "list": "LIST_OF_DEVICES",

}

rules = {
    "<HELLO> -> hi": [("Term", "hi")],
    "<HELLO> -> hello": [("Term", "hello")],
    "<LIST_OF_DEVICES> -> <LIST><ARTICLE><DEVICES>": [("NonT", "LIST"), ("NonT", "ARTICLE"), ("NonT", "DEVICES")],
}


def lexical_analysis(message):
    message = [x.lower() for x in message.split()]
    tokens = []
    for token in message:
        if token in TERMS:
            tokens.append(token)
        else:
            tokens.append("INVALID")
    tokens.append("END")
    return tokens


def syntactic_analysis(message):
    tokens = lexical_analysis(message)
    type_of_message = reverse_table[tokens[0]]
    stack = [("Term", "END"), ("NonT", type_of_message)]
    position = 0
    while len(stack) > 0:
        (stype, svalue) = stack.pop()
        token = tokens[position]
        if stype == "Term":
            if svalue == token:
                position += 1
                if token == "END":
                    return reverse_table[tokens[0]]
            else:
                return "ERROR"
        elif stype == "NonT":
            rule = table[svalue][token]
            if rule != -1:
                for r in reversed(rules[rule]):
                    stack.append(r)
            else:
                return "ERROR"
