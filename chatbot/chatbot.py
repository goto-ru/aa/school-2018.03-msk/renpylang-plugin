#!/usr/bin/env python
# -*- coding: utf-8 -*-

from telegram.ext import Updater, CommandHandler, MessageHandler, Filters
from config import TOKEN
from understand import syntactic_analysis

class Chatbot():
    "Our chatbot"\

    def __init__(self):
        self._state = "IDLE"
        self._event = ""

    def understand(self, message):
        print("Message: {0}".format(message))
        type_of_message = syntactic_analysis(message)
        print("Type: {0}".format(type_of_message))
        if self._state == "IDLE":
            if type_of_message == "HELLO":
                self._event = "HELLO"
            else:
                self._event = "ERROR"

    def generate(self):
        if self._event == "ERROR":
            return "I can't understand you"
        if self._state == "IDLE":
            if self._event == "HELLO":
                return "Hello"

    def process(self, message):
        self.understand(message) 
        return self.generate()

    def process_tg(self, bot, update):
        update.message.reply_text(self.process(update.message.text))

    def error(self, bot, update, error):
        print('Update "%s" caused error "%s"' % (update, error))

    def start_tg(self, bot, update):
        update.message.reply_text(self. generate())

    def run(self, tg=True):
        """Start the bot."""
        if tg:
            updater = Updater(TOKEN)
            dp = updater.dispatcher
            dp.add_handler(CommandHandler("start", self.start_tg))
            dp.add_handler(MessageHandler(Filters.text, self.process_tg))

            dp.add_error_handler(self.error)
            updater.start_polling()

            print("Tg started")

            updater.idle()
        while True:
            print(self.generate())
            self.understand(input().strip())


if __name__ == '__main__':
    chat = Chatbot()
    chat.run(True)
