renpylang-plugin
================
A Plugin for IntelliJ IDEA to enable Ren'py support.  
What is Ren'py: https://www.renpy.org/  
Presentation of project: https://goo.gl/VTHWFH  
Author: Ромащенко Владимир (Romashchenko Vladimir)  
