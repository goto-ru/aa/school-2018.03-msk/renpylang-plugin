// This is a generated file. Not intended for manual editing.
package com.renpyplugin.psi.impl;

import java.util.List;
import org.jetbrains.annotations.*;
import com.intellij.lang.ASTNode;
import com.intellij.psi.PsiElement;
import com.intellij.psi.PsiElementVisitor;
import com.intellij.psi.util.PsiTreeUtil;
import static com.renpyplugin.psi.RenpyTypes.*;
import com.intellij.extapi.psi.ASTWrapperPsiElement;
import com.renpyplugin.psi.*;

public class RenpyDefImpl extends ASTWrapperPsiElement implements RenpyDef {

  public RenpyDefImpl(ASTNode node) {
    super(node);
  }

  public void accept(@NotNull RenpyVisitor visitor) {
    visitor.visitDef(this);
  }

  public void accept(@NotNull PsiElementVisitor visitor) {
    if (visitor instanceof RenpyVisitor) accept((RenpyVisitor)visitor);
    else super.accept(visitor);
  }

  @Override
  @NotNull
  public RenpyVar getVar() {
    return findNotNullChildByClass(RenpyVar.class);
  }

}
