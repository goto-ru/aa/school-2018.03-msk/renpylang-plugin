// This is a generated file. Not intended for manual editing.
package com.renpyplugin.psi;

import org.jetbrains.annotations.*;
import com.intellij.psi.PsiElementVisitor;
import com.intellij.psi.PsiElement;

public class RenpyVisitor extends PsiElementVisitor {

  public void visitBlock(@NotNull RenpyBlock o) {
    visitPsiElement(o);
  }

  public void visitDef(@NotNull RenpyDef o) {
    visitPsiElement(o);
  }

  public void visitFunction(@NotNull RenpyFunction o) {
    visitPsiElement(o);
  }

  public void visitParams(@NotNull RenpyParams o) {
    visitPsiElement(o);
  }

  public void visitShw(@NotNull RenpyShw o) {
    visitPsiElement(o);
  }

  public void visitVar(@NotNull RenpyVar o) {
    visitPsiElement(o);
  }

  public void visitPsiElement(@NotNull PsiElement o) {
    visitElement(o);
  }

}
