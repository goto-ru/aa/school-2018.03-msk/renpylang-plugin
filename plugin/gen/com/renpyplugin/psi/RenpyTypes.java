// This is a generated file. Not intended for manual editing.
package com.renpyplugin.psi;

import com.intellij.psi.tree.IElementType;
import com.intellij.psi.PsiElement;
import com.intellij.lang.ASTNode;
import com.renpyplugin.psi.impl.*;

public interface RenpyTypes {

  IElementType BLOCK = new RenpyElementType("BLOCK");
  IElementType DEF = new RenpyElementType("DEF");
  IElementType FUNCTION = new RenpyElementType("FUNCTION");
  IElementType PARAMS = new RenpyElementType("PARAMS");
  IElementType SHW = new RenpyElementType("SHW");
  IElementType VAR = new RenpyElementType("VAR");

  IElementType BLOCK_NAME = new RenpyTokenType("BLOCK_NAME");
  IElementType BLOCK_START = new RenpyTokenType("BLOCK_START");
  IElementType COMMENT = new RenpyTokenType("COMMENT");
  IElementType CRLF = new RenpyTokenType("CRLF");
  IElementType DEFINE = new RenpyTokenType("DEFINE");
  IElementType FUNCTION_CALL = new RenpyTokenType("FUNCTION_CALL");
  IElementType PARAM_NAME = new RenpyTokenType("PARAM_NAME");
  IElementType SEPARATOR = new RenpyTokenType("SEPARATOR");
  IElementType SHOW = new RenpyTokenType("SHOW");
  IElementType STRING = new RenpyTokenType("STRING");
  IElementType TAG = new RenpyTokenType("TAG");
  IElementType VALUE = new RenpyTokenType("VALUE");
  IElementType VARIABLE = new RenpyTokenType("VARIABLE");
  IElementType VARIABLE1 = new RenpyTokenType("VARIABLE1");
  IElementType VARIABLE2 = new RenpyTokenType("VARIABLE2");
  IElementType VARIABLE3 = new RenpyTokenType("VARIABLE3");

  class Factory {
    public static PsiElement createElement(ASTNode node) {
      IElementType type = node.getElementType();
       if (type == BLOCK) {
        return new RenpyBlockImpl(node);
      }
      else if (type == DEF) {
        return new RenpyDefImpl(node);
      }
      else if (type == FUNCTION) {
        return new RenpyFunctionImpl(node);
      }
      else if (type == PARAMS) {
        return new RenpyParamsImpl(node);
      }
      else if (type == SHW) {
        return new RenpyShwImpl(node);
      }
      else if (type == VAR) {
        return new RenpyVarImpl(node);
      }
      throw new AssertionError("Unknown element type: " + type);
    }
  }
}
