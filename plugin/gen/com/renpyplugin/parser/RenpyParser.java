// This is a generated file. Not intended for manual editing.
package com.renpyplugin.parser;

import com.intellij.lang.PsiBuilder;
import com.intellij.lang.PsiBuilder.Marker;
import static com.renpyplugin.psi.RenpyTypes.*;
import static com.intellij.lang.parser.GeneratedParserUtilBase.*;
import com.intellij.psi.tree.IElementType;
import com.intellij.lang.ASTNode;
import com.intellij.psi.tree.TokenSet;
import com.intellij.lang.PsiParser;
import com.intellij.lang.LightPsiParser;

@SuppressWarnings({"SimplifiableIfStatement", "UnusedAssignment"})
public class RenpyParser implements PsiParser, LightPsiParser {

  public ASTNode parse(IElementType t, PsiBuilder b) {
    parseLight(t, b);
    return b.getTreeBuilt();
  }

  public void parseLight(IElementType t, PsiBuilder b) {
    boolean r;
    b = adapt_builder_(t, b, this, null);
    Marker m = enter_section_(b, 0, _COLLAPSE_, null);
    if (t == BLOCK) {
      r = block(b, 0);
    }
    else if (t == DEF) {
      r = def(b, 0);
    }
    else if (t == FUNCTION) {
      r = function(b, 0);
    }
    else if (t == PARAMS) {
      r = params(b, 0);
    }
    else if (t == SHW) {
      r = shw(b, 0);
    }
    else if (t == VAR) {
      r = var(b, 0);
    }
    else {
      r = parse_root_(t, b, 0);
    }
    exit_section_(b, 0, m, t, r, true, TRUE_CONDITION);
  }

  protected boolean parse_root_(IElementType t, PsiBuilder b, int l) {
    return renpyFile(b, l + 1);
  }

  /* ********************************************************** */
  // (BLOCK_START BLOCK_NAME)|BLOCK_START
  public static boolean block(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "block")) return false;
    if (!nextTokenIs(b, BLOCK_START)) return false;
    boolean r;
    Marker m = enter_section_(b);
    r = block_0(b, l + 1);
    if (!r) r = consumeToken(b, BLOCK_START);
    exit_section_(b, m, BLOCK, r);
    return r;
  }

  // BLOCK_START BLOCK_NAME
  private static boolean block_0(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "block_0")) return false;
    boolean r;
    Marker m = enter_section_(b);
    r = consumeTokens(b, 0, BLOCK_START, BLOCK_NAME);
    exit_section_(b, m, null, r);
    return r;
  }

  /* ********************************************************** */
  // DEFINE var TAG* SEPARATOR?
  public static boolean def(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "def")) return false;
    if (!nextTokenIs(b, DEFINE)) return false;
    boolean r;
    Marker m = enter_section_(b);
    r = consumeToken(b, DEFINE);
    r = r && var(b, l + 1);
    r = r && def_2(b, l + 1);
    r = r && def_3(b, l + 1);
    exit_section_(b, m, DEF, r);
    return r;
  }

  // TAG*
  private static boolean def_2(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "def_2")) return false;
    int c = current_position_(b);
    while (true) {
      if (!consumeToken(b, TAG)) break;
      if (!empty_element_parsed_guard_(b, "def_2", c)) break;
      c = current_position_(b);
    }
    return true;
  }

  // SEPARATOR?
  private static boolean def_3(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "def_3")) return false;
    consumeToken(b, SEPARATOR);
    return true;
  }

  /* ********************************************************** */
  // FUNCTION_CALL params
  public static boolean function(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "function")) return false;
    if (!nextTokenIs(b, FUNCTION_CALL)) return false;
    boolean r;
    Marker m = enter_section_(b);
    r = consumeToken(b, FUNCTION_CALL);
    r = r && params(b, l + 1);
    exit_section_(b, m, FUNCTION, r);
    return r;
  }

  /* ********************************************************** */
  // VARIABLE|STRING|COMMENT|CRLF|def|shw|function|block
  static boolean item_(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "item_")) return false;
    boolean r;
    Marker m = enter_section_(b);
    r = consumeToken(b, VARIABLE);
    if (!r) r = consumeToken(b, STRING);
    if (!r) r = consumeToken(b, COMMENT);
    if (!r) r = consumeToken(b, CRLF);
    if (!r) r = def(b, l + 1);
    if (!r) r = shw(b, l + 1);
    if (!r) r = function(b, l + 1);
    if (!r) r = block(b, l + 1);
    exit_section_(b, m, null, r);
    return r;
  }

  /* ********************************************************** */
  // (PARAM_NAME VALUE)|VALUE
  public static boolean params(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "params")) return false;
    if (!nextTokenIs(b, "<params>", PARAM_NAME, VALUE)) return false;
    boolean r;
    Marker m = enter_section_(b, l, _NONE_, PARAMS, "<params>");
    r = params_0(b, l + 1);
    if (!r) r = consumeToken(b, VALUE);
    exit_section_(b, l, m, r, false, null);
    return r;
  }

  // PARAM_NAME VALUE
  private static boolean params_0(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "params_0")) return false;
    boolean r;
    Marker m = enter_section_(b);
    r = consumeTokens(b, 0, PARAM_NAME, VALUE);
    exit_section_(b, m, null, r);
    return r;
  }

  /* ********************************************************** */
  // item_*
  static boolean renpyFile(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "renpyFile")) return false;
    int c = current_position_(b);
    while (true) {
      if (!item_(b, l + 1)) break;
      if (!empty_element_parsed_guard_(b, "renpyFile", c)) break;
      c = current_position_(b);
    }
    return true;
  }

  /* ********************************************************** */
  // SHOW var TAG*
  public static boolean shw(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "shw")) return false;
    if (!nextTokenIs(b, SHOW)) return false;
    boolean r;
    Marker m = enter_section_(b);
    r = consumeToken(b, SHOW);
    r = r && var(b, l + 1);
    r = r && shw_2(b, l + 1);
    exit_section_(b, m, SHW, r);
    return r;
  }

  // TAG*
  private static boolean shw_2(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "shw_2")) return false;
    int c = current_position_(b);
    while (true) {
      if (!consumeToken(b, TAG)) break;
      if (!empty_element_parsed_guard_(b, "shw_2", c)) break;
      c = current_position_(b);
    }
    return true;
  }

  /* ********************************************************** */
  // VARIABLE|VARIABLE1|VARIABLE2|VARIABLE3
  public static boolean var(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "var")) return false;
    boolean r;
    Marker m = enter_section_(b, l, _NONE_, VAR, "<var>");
    r = consumeToken(b, VARIABLE);
    if (!r) r = consumeToken(b, VARIABLE1);
    if (!r) r = consumeToken(b, VARIABLE2);
    if (!r) r = consumeToken(b, VARIABLE3);
    exit_section_(b, l, m, r, false, null);
    return r;
  }

}
