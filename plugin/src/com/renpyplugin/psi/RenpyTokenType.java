package com.renpyplugin.psi;

import com.intellij.psi.tree.IElementType;
import com.renpyplugin.RenpyLanguage;
import org.jetbrains.annotations.*;

public class RenpyTokenType extends IElementType {
    public RenpyTokenType(@NotNull @NonNls String debugName) {
        super(debugName, RenpyLanguage.INSTANCE);
    }

    @Override
    public String toString() {
        return "RenpyTokenType." + super.toString();
    }
}
