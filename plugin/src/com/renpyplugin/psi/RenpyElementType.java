package com.renpyplugin.psi;

import com.intellij.psi.tree.IElementType;
import com.renpyplugin.RenpyLanguage;
import org.jetbrains.annotations.*;

public class RenpyElementType extends IElementType {
    public RenpyElementType(@NotNull @NonNls String debugName) {
        super(debugName, RenpyLanguage.INSTANCE);
    }
}