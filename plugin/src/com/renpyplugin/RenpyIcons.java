package com.renpyplugin;

import com.intellij.openapi.util.IconLoader;

import javax.swing.*;

public class RenpyIcons {
    public static final Icon FILE = IconLoader.getIcon("/com/renpyplugin/icons/rpy.png");
}