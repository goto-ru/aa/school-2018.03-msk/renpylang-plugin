package com.renpyplugin;

import com.intellij.lexer.FlexLexer;
import com.intellij.psi.tree.IElementType;
import com.renpyplugin.psi.RenpyTypes;
import com.intellij.psi.TokenType;

%%

%class RenpyLexer
%implements FlexLexer
%unicode
%function advance
%type IElementType
%eof{  return;
%eof}

CRLF=\r\n|[\r\n\u2028\u2029\u000B\u000C\u0085]
WS=[\ \n\t\f]
NAME=(\w+)\s*
COMMENT=("#")[^\r\n]*
STRING=\"(\\[^\r]|[^\"\r])*\"
DEFINE=\s*(image|define|default)\s+
SHOW=(scene|show)\s+
BLOCK_START=\s*(label|init)\s*
FUNCTION_START=(\w+\.?)+\(
VARIABLE_DEFENITION_START=\$?\s*(\w+\.)
VARIABLE_DEFENITION=\$?\s*(\w+\.?)\s*=\s*
BOOL=(True|False)

%state WAITING_VARIABLE_NAME_DEFINE
%state WAITING_VARIABLE_NAME_SHOW
%state WAITING_TAGS_DEFINE
%state WAITING_TAGS_SHOW
%state WAITING_BLOCK_NAME
%state WAITING_PARAMS
%state WAITING_VALUE
%state CLASS1
%state CLASS2
%state CLASS3
%%

<YYINITIAL> {
    {COMMENT}                                               { return RenpyTypes.COMMENT; }
    {STRING}                                                { return RenpyTypes.STRING; }
    {VARIABLE_DEFENITION_START}                             { yybegin(CLASS1); return RenpyTypes.VARIABLE1; }
    {VARIABLE_DEFENITION}                                   { yybegin(WAITING_VALUE); return RenpyTypes.VARIABLE1; }
    {FUNCTION_START}                                        { yybegin(WAITING_PARAMS); return RenpyTypes.FUNCTION_CALL; }
    {BLOCK_START}                                           { yybegin(WAITING_BLOCK_NAME); return RenpyTypes.BLOCK_START; }
    \s*{SHOW}                                               { yybegin(WAITING_VARIABLE_NAME_SHOW); return RenpyTypes.DEFINE;  }
    ^{DEFINE}                                               { yybegin(WAITING_VARIABLE_NAME_DEFINE); return RenpyTypes.DEFINE; }
}
<CLASS1> {
    \w+\s*=\s*                                              { yybegin(WAITING_VALUE); return RenpyTypes.VARIABLE2; }
    \w+\.                                                   { yybegin(CLASS2); return RenpyTypes.VARIABLE2; }
}
<CLASS2> {
    \w+\s*=\s*                                              { yybegin(WAITING_VALUE); return RenpyTypes.VARIABLE3; }
    \w+\.                                                   { yybegin(CLASS3); return RenpyTypes.VARIABLE3; }
}
<CLASS3> {
    \w+\s*=\s*                                              { yybegin(WAITING_VALUE); return RenpyTypes.VARIABLE1; }
    \w+\.                                                   { yybegin(CLASS1); return RenpyTypes.VARIABLE1; }
}
<WAITING_VARIABLE_NAME_SHOW>{
    {NAME}                                                  { yybegin(WAITING_TAGS_SHOW); return RenpyTypes.VARIABLE; }
}
<WAITING_VARIABLE_NAME_DEFINE>{
    {NAME}                                                  { yybegin(WAITING_TAGS_DEFINE); return RenpyTypes.VARIABLE; }
}
<WAITING_TAGS_DEFINE> {
    {NAME}                                                  { return RenpyTypes.TAG; }
    \=                                                      { yybegin(YYINITIAL); return RenpyTypes.SEPARATOR; }
}
<WAITING_TAGS_SHOW> {
    \w+                                                     { return RenpyTypes.TAG; }
    \w+{CRLF}                                               { yybegin(YYINITIAL); return RenpyTypes.TAG; }
}
<WAITING_BLOCK_NAME> {NAME}":"                              { return RenpyTypes.BLOCK_NAME; }
<WAITING_PARAMS> {
    {NAME}\=                                                { return RenpyTypes.PARAM_NAME; }
    {NAME}                                                  { return RenpyTypes.VALUE; }
    {STRING}                                                { return RenpyTypes.STRING; }
    \,\s*                                                   { return RenpyTypes.SEPARATOR; }
    ")"                                                     { yybegin(YYINITIAL); return RenpyTypes.FUNCTION_CALL; }
}
<WAITING_VALUE> {
    {STRING}$                                               { yybegin(YYINITIAL); return RenpyTypes.STRING; }
    {BOOL}|{NAME}$                                          { yybegin(YYINITIAL); return RenpyTypes.VALUE; }
}

({CRLF}|{WS})+                                              { yybegin(YYINITIAL); return TokenType.WHITE_SPACE; }
.                                                           { return TokenType.BAD_CHARACTER; }
