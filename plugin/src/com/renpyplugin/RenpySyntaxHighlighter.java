package com.renpyplugin;

import com.intellij.lexer.Lexer;
import com.intellij.openapi.editor.*;
import com.intellij.openapi.editor.colors.TextAttributesKey;
import com.intellij.openapi.fileTypes.SyntaxHighlighterBase;
import com.intellij.psi.TokenType;
import com.intellij.psi.tree.IElementType;
import com.renpyplugin.psi.RenpyTypes;
import org.jetbrains.annotations.NotNull;

import static com.intellij.openapi.editor.colors.TextAttributesKey.createTextAttributesKey;

public class RenpySyntaxHighlighter extends SyntaxHighlighterBase {
    public static final TextAttributesKey SEPARATOR =
            createTextAttributesKey("RENPY_SEPARATOR", DefaultLanguageHighlighterColors.OPERATION_SIGN);
    public static final TextAttributesKey COMMENT =
            createTextAttributesKey("RENPY_COMMENT", DefaultLanguageHighlighterColors.LINE_COMMENT);
    public static final TextAttributesKey STRING =
            createTextAttributesKey("RENPY_STRING", DefaultLanguageHighlighterColors.STRING);
    public static final TextAttributesKey DEFINE =
            createTextAttributesKey("RENPY_DEFINE", DefaultLanguageHighlighterColors.STATIC_FIELD);
    public static final TextAttributesKey VARIABLE =
            createTextAttributesKey("RENPY_VARIABLE", DefaultLanguageHighlighterColors.GLOBAL_VARIABLE);
    public static final TextAttributesKey VARIABLE1 =
            createTextAttributesKey("RENPY_VARIABLE1", DefaultLanguageHighlighterColors.INSTANCE_METHOD);
    public static final TextAttributesKey VARIABLE2 =
            createTextAttributesKey("RENPY_VARIABLE2", DefaultLanguageHighlighterColors.KEYWORD);
    public static final TextAttributesKey VARIABLE3 =
            createTextAttributesKey("RENPY_VARIABLE3", DefaultLanguageHighlighterColors.IDENTIFIER);
    public static final TextAttributesKey TAG =
            createTextAttributesKey("RENPY_TAG", DefaultLanguageHighlighterColors.KEYWORD);
    public static final TextAttributesKey BLOCK_START =
            createTextAttributesKey("RENPY_BLOCK_START", DefaultLanguageHighlighterColors.STATIC_FIELD);
    public static final TextAttributesKey BLOCK_NAME =
            createTextAttributesKey("RENPY_BLOCK_NAME", DefaultLanguageHighlighterColors.GLOBAL_VARIABLE);
    public static final TextAttributesKey PARAM_NAME =
            createTextAttributesKey("PARAM_NAME_NAME", DefaultLanguageHighlighterColors.INLINE_PARAMETER_HINT_CURRENT);
    public static final TextAttributesKey BAD_CHARACTER =
            createTextAttributesKey("RENPY_BAD_CHARACTER", HighlighterColors.BAD_CHARACTER);

    private static final TextAttributesKey[] BAD_CHAR_KEYS = new TextAttributesKey[]{BAD_CHARACTER};
    private static final TextAttributesKey[] SEPARATOR_KEYS = new TextAttributesKey[]{SEPARATOR};
    private static final TextAttributesKey[] COMMENT_KEYS = new TextAttributesKey[]{COMMENT};
    private static final TextAttributesKey[] STRING_KEYS = new TextAttributesKey[]{STRING};
    private static final TextAttributesKey[] DEFINE_KEYS = new TextAttributesKey[]{DEFINE};
    private static final TextAttributesKey[] VARIABLE_KEYS = new TextAttributesKey[]{VARIABLE};
    private static final TextAttributesKey[] VARIABLE1_KEYS = new TextAttributesKey[]{VARIABLE1};
    private static final TextAttributesKey[] VARIABLE2_KEYS = new TextAttributesKey[]{VARIABLE2};
    private static final TextAttributesKey[] VARIABLE3_KEYS = new TextAttributesKey[]{VARIABLE3};
    private static final TextAttributesKey[] TAG_KEYS = new TextAttributesKey[]{TAG};
    private static final TextAttributesKey[] BLOCK_START_KEYS = new TextAttributesKey[]{BLOCK_START};
    private static final TextAttributesKey[] BLOCK_NAME_KEYS = new TextAttributesKey[]{BLOCK_NAME};
    private static final TextAttributesKey[] PARAM_NAME_KEYS = new TextAttributesKey[]{PARAM_NAME};
    private static final TextAttributesKey[] EMPTY_KEYS = new TextAttributesKey[0];

    @NotNull
    @Override
    public Lexer getHighlightingLexer() {
        return new RenpyLexerAdapter();
    }

    @NotNull
    @Override
    public TextAttributesKey[] getTokenHighlights(IElementType tokenType) {
        if (tokenType.equals(RenpyTypes.SEPARATOR)) {
            return SEPARATOR_KEYS;
        } else if (tokenType.equals(RenpyTypes.COMMENT)) {
            return COMMENT_KEYS;
        } else if (tokenType.equals(RenpyTypes.STRING)) {
            return STRING_KEYS;
        } else if (tokenType.equals(RenpyTypes.DEFINE)) {
            return DEFINE_KEYS;
        } else if (tokenType.equals(RenpyTypes.VARIABLE)) {
            return VARIABLE_KEYS;
        } else if (tokenType.equals(RenpyTypes.VARIABLE1)) {
            return VARIABLE1_KEYS;
        } else if (tokenType.equals(RenpyTypes.VARIABLE2)) {
            return VARIABLE2_KEYS;
        } else if (tokenType.equals(RenpyTypes.VARIABLE3)) {
            return VARIABLE3_KEYS;
        } else if (tokenType.equals(RenpyTypes.TAG)) {
            return TAG_KEYS;
        } else if (tokenType.equals(RenpyTypes.BLOCK_START)) {
            return BLOCK_START_KEYS;
        } else if (tokenType.equals(RenpyTypes.BLOCK_NAME)) {
            return BLOCK_NAME_KEYS;
        } else if (tokenType.equals(RenpyTypes.PARAM_NAME)) {
            return PARAM_NAME_KEYS;
        } else if (tokenType.equals(TokenType.BAD_CHARACTER)) {
            return BAD_CHAR_KEYS;
        } else {
            return EMPTY_KEYS;
        }
    }
}
