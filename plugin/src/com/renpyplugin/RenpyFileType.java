package com.renpyplugin;

import com.intellij.openapi.fileTypes.LanguageFileType;
import org.jetbrains.annotations.*;

import javax.swing.*;

public class RenpyFileType extends LanguageFileType {
    public static final RenpyFileType INSTANCE = new RenpyFileType();

    private RenpyFileType() {
        super(RenpyLanguage.INSTANCE);
    }

    @NotNull
    @Override
    public String getName() {
        return "Renpy file";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Renpy language file";
    }

    @NotNull
    @Override
    public String getDefaultExtension() {
        return "rpy";
    }

    @Nullable
    @Override
    public Icon getIcon() {
        return RenpyIcons.FILE;
    }
}